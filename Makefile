V=20221221

PREFIX = /usr/local

install:
	install -dm755 $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
		install -m0644 firerain{.gpg,-trusted,-revoked} $(DESTDIR)$(PREFIX)/share/pacman/keyrings/

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/share/pacman/keyrings/firerain{.gpg,-trusted,-revoked}
	rmdir -p --ignore-fail-on-non-empty $(DESTDIR)$(PREFIX)/share/pacman/keyrings/

dist:
	git archive --format=tar --prefix=firerain-keyring-$(V)/ $(V) | gzip -9 > firerain-keyring-$(V).tar.gz
	gpg --detach-sign --use-agent firerain-keyring-$(V).tar.gz

.PHONY: install uninstall dist 
